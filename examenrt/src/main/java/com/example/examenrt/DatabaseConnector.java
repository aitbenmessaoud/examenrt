package com.example.examenrt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class DatabaseConnector implements CommandLineRunner {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public void run(String... args) throws Exception {
        String sql = "SELECT * FROM Purchasing.ProductVendor";
        List<String> customers = jdbcTemplate.query(sql,
                BeanPropertyRowMapper.newInstance(String.class));

        customers.forEach(System.out :: println);
    }
}

//shippers venders produit_vendor Purchasing.ProductVendor
//orders sales.order.header
//customer sales.customer
//employees humanresource.employees