package com.example.examenrt;

public interface IKafkaConfig {
    public static String KAFKA_BROKERS = "localhost:9092";

    public static String CLIENT_ID =  "client1";

    public static String TOPIC_NAME = "third_topic";

    public static String GROUP_ID = "com.example.App";

    public static String OFFSET_RESET_EARLIER = "earliest";

}
