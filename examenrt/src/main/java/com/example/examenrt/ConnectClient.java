package com.example.examenrt;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.util.Properties;

public class ConnectClient {

    public static KafkaStreams createStream() {
        Properties props = new Properties();
        props.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "table-join-application");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");


        StreamsBuilder builder = new StreamsBuilder();

        KStream<String, String> table1Stream = builder.stream("Sales.Customer");
        KStream<String, String> table2Stream = builder.stream("Sales.OrderHeader");


        // Join tables based on key
        KStream<String, String> joinedStream = table1Stream.join(
                table2Stream,
                (value1, value2) -> "table1Value=" + value1 + ", table2Value=" + value2,
                JoinWindows.of(Duration.ofMinutes(5))
        );

        joinedStream.to("joined-tables-topic");

        KafkaStreams streams = new KafkaStreams(builder.build(), props);
        streams.start();
        return streams;
    }
}
