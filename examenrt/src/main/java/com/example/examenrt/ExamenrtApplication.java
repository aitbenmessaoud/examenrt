package com.example.examenrt;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;

@SpringBootApplication
public class ExamenrtApplication {

	public static void main(String[] args) {
		//run(ExamenrtApplication.class, args);
		//runStream();
		//runConsumers();
	}
	public static void runStream(){
		try{
			ConnectClient.createStream();
			System.out.println("stream started !");
		}catch (Exception exception){
			System.out.println("stream failed");
		}
	}

	public static void runConsumers(){
		try (Consumer<Long, Vendor> consumer = ConsumerVendor.createConsumer();
			 BufferedWriter writer = new BufferedWriter(new FileWriter("messages.csv"))) {
			consumer.subscribe(Arrays.asList(IKafkaConfig.TOPIC_NAME));
			while (true){
				ConsumerRecords<Long ,Vendor> records = consumer.poll(Duration.ofMillis(100));
				for(ConsumerRecord<Long, Vendor> record : records){
					String message = record.value().toString();
					System.out.println("Message received : "+ record.value().toString());
					writer.write(message);
					writer.newLine();
				}
				writer.flush();
			}
		} catch (IOException exception) {
			System.out.println("Error writing to CSV file: " + exception.getMessage());
		} catch (Exception exception) {
			System.out.println("Consumer failed: " + exception.getMessage());
		}
	}
}
