package com.example.examenrt.batchdetection.entity;

public record Employee(
        int id,
        String name){
}
