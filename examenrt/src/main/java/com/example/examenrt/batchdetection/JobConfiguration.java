package com.example.examenrt.batchdetection;


import com.example.examenrt.batchdetection.entity.Employee;
import com.example.examenrt.batchdetection.processing.FileTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.support.JdbcTransactionManager;

import javax.sql.DataSource;

@Configuration
public class JobConfiguration {



    @Bean
    public Job job(JobRepository jobRepository, Step step1, Step step2){
        return new JobBuilder("employeeJob", jobRepository)
                .start(step1)
                .next(step2)
                .build();
    }

    @Bean
    public Step step1(JobRepository jobRepository, JdbcTransactionManager transactionManager){
        return new StepBuilder("filepreparation", jobRepository)
                .tasklet(new FileTasklet(), transactionManager)
                .build();
    }

    @Bean
    public Step step2(JobRepository jobRepository, JdbcTransactionManager transactionManager,
                      ItemReader<Employee> employeeDataFileReader,
                      ItemWriter<Employee> employeeDataTableWriter){
        return new StepBuilder("fileingetion", jobRepository)
                .<Employee, Employee>chunk(100, transactionManager)
                .reader(employeeDataFileReader)
                .writer(employeeDataTableWriter)
                .faultTolerant()
                .skip(FlatFileParseException.class)
                .skipLimit(10)
                .build();
    }

    @Bean
    @StepScope
    public FlatFileItemReader<Employee> employeeDataFileReader(@Value("#{jobParameters['input.file']}") String inputFile){
        return new FlatFileItemReaderBuilder<Employee>()
                .name("employeeDataFileReader")
                .resource(new FileSystemResource(inputFile))
                .delimited()
                .names("id", "name")
                .targetType(Employee.class)
                .build();

    }

    @Bean
    public JdbcBatchItemWriter<Employee> employeeDataTableWriter(DataSource dataSource){
        String sql ="insert into EMPLOYEE_DATA values (:id, :name)";
        return new JdbcBatchItemWriterBuilder<Employee>()
                .dataSource(dataSource)
                .sql(sql)
                .beanMapped()
                .build();
    }
}
